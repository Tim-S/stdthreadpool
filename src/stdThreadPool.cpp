#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>

#include <iostream>
using namespace std;

queue<function<void()>> tasks;
mutex tasks_mutex;
condition_variable condition;

bool done = false;

void worker() {
	while (!done) {
		function<void()> task;
		{
			unique_lock<mutex> lock(tasks_mutex);
			condition.wait(lock, [] {return (!tasks.empty()) || done;});
			if(done) return;

			task = tasks.front();
			tasks.pop();
		}
		task();
	}
}

void addTask(function<void()> task){
	{
		unique_lock<mutex> lock(tasks_mutex);
		tasks.push(task);
	}
	condition.notify_one();
}

mutex log_mutex;
void log(string& msg){
	unique_lock<mutex> lock(log_mutex);
	cout <<"Thread #"<<this_thread::get_id()<<" logged \""<<msg<<"\""<<endl;
}

int main() {
	int concurrentThreads = thread::hardware_concurrency();
	int numWorkers = concurrentThreads;
	cout << "Concurrent threads: "<< concurrentThreads << endl;
	cout << "Using "<<numWorkers<< " Workers." << endl;
	vector<thread> pool;

	for (int i = 0; i < numWorkers; ++i) {
		pool.push_back(thread(worker));
	}

	auto msg = string("Hello World!");
	for (int i = 0; i < 1000; ++i){
		addTask(bind(&log, msg));
	}

	//wait for all scheduled tasks to be completed
	while(!tasks.empty()){
		this_thread::sleep_for(std::chrono::seconds(1));
	}
	done = true;
	condition.notify_all();
	cout << "done!" << endl;

	for (int i = 0; i < numWorkers; ++i) {
		pool[i].join();
	}
	return 0;
}
