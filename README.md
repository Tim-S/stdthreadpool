# stdThreadPool
A simple example for Thread-Pools using C++11 standard features only.

## Example Output
```shell

Concurrent threads: 8
Using 8 Workers.Thread #5 logged "Hello World!"
Thread #2 logged "Hello World!"
Thread #4 logged "Hello World!"
Thread #2 logged "Hello World!"
Thread #6 logged "Hello World!"
Thread #3 logged "Hello World!"
Thread #5 logged "Hello World!"
Thread #7 logged "Hello World!"
Thread #4 logged "Hello World!"
Thread #2 logged "Hello World!"
Thread #8 logged "Hello World!"
Thread #6 logged "Hello World!"
...
Thread #8 logged "Hello World!"
done!

```